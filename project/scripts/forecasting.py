from pandas import DataFrame
from statsmodels.tsa.arima.model import ARIMA
from pandas.tseries.offsets import DateOffset
import math
import numpy as np
import pandas as pd

def forecasting(data, period):
    data_len = len(data['data'])
    df = DataFrame(data=data)

    df.columns = ['data', 'hba1c']
    df['data'] = pd.to_datetime(df['data'])
    
    for i in range(1, data_len):
        df['data'][i] = df['data'][i - 1] + DateOffset(months=3)
    df.set_index('data', inplace=True)

    arima = ARIMA(df.values, order=(3, 1, 1))
    trained_arima = arima.fit()
    results = trained_arima.forecast(steps=3)
    
    estimations_dates = [df.index[-1] + DateOffset(months=m)for m in range(0, int(period)*4, int(period))]
    estimations_dates_df = pd.DataFrame(index=estimations_dates[1:], columns=df.columns)
    estimations_df = pd.concat([df, estimations_dates_df])
    
    estimations_df['forecast'] = np.nan
    estimations_df['forecast'][data_len - 1] =  estimations_df['hba1c'][data_len - 1]
    for i in range(0, 3):
        estimations_df['forecast'][data_len + i] =  results[i]
        
    return transform_to_dict(estimations_df)

def transform_to_dict(df):
    data = {}
    labels = []
    for key, val in df.to_dict().items():
        tmp = []
        
        for timestamp, val_forecasting in val.items():
            if not math.isnan(val_forecasting):
                tmp.append(round(val_forecasting, 2))
                str_time = timestamp.strftime('%Y-%m')
                
                if not str_time in labels:
                    labels.append(str_time)
            else:
                tmp.append('null')
        data[key] = tmp
    return data, labels
    
   
