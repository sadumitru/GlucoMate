import os
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler as Scaler
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn import model_selection
from xgboost.sklearn import XGBClassifier

scripts_dir = os.path.dirname(__file__)
root_dir = os.path.abspath(os.path.join(scripts_dir, os.pardir))

def classifier(categories):
    data = pd.read_csv(f'{root_dir}/full_dataset.tsv', sep='\t')

    # categorical data
    cat_sub_columns = list(categories.keys())
    cat_columns = cat_sub_columns.copy()
    cat_columns.append("HbA1c_cat")
    cat_data_subset = data[cat_columns].copy()
    cat_data_subset.dropna(inplace=True)

    # split cateforic data
    cat_train_set, cat_test_set, cat_train_label, cat_test_label = train_test_split(cat_data_subset[cat_sub_columns], cat_data_subset['HbA1c_cat'],
                                                              test_size=0.33,random_state=42)

    cat_scaler = Scaler()
    cat_scaler.fit(cat_train_set)
    cat_train_set_scaled = cat_scaler.transform(cat_train_set)
    cat_test_set_scaled = cat_scaler.transform(cat_test_set)
    cat_user_vals = pd.DataFrame(categories)
    cat_user_vals_scaled = cat_scaler.transform(cat_user_vals)

    # array of different models
    models = []
    models.append(('LR', LogisticRegression(solver ='lbfgs',multi_class='auto')))
    models.append(('KNN', KNeighborsClassifier()))
    models.append(('NB', GaussianNB()))
    models.append(('SVC', SVC(gamma='scale')))
    models.append(('RFC', RandomForestClassifier(n_estimators=100)))
    models.append(('XGB', XGBClassifier()))

    results = []
    X_cat = cat_train_set_scaled
    y_cat = cat_train_label
    seed = 5

    for name, model in models:
        kfold = model_selection.KFold(n_splits = 10, random_state=seed, shuffle=True)
        if name == "XGB":
          le = preprocessing.LabelEncoder()
          xbg_cat_train_label =  le.fit_transform(y_cat)
          cv_results = model_selection.cross_val_score(model, X_cat, xbg_cat_train_label, cv=kfold, scoring='accuracy')
        else:
          cv_results = model_selection.cross_val_score(model, X_cat, y_cat, cv=kfold, scoring='accuracy')
        results.append((name, cv_results.std()))

    results.sort(key=lambda y: y[1])
    best_model = results[0][0]

    if best_model == 'LR':
      model = LogisticRegression(solver ='lbfgs',multi_class='auto')
    elif best_model == 'KNN':
      model = KNeighborsClassifier()
    elif best_model == 'NB':
      model = GaussianNB()
    elif best_model == 'SVC':
      model = SVC(gamma='scale')
    elif best_model == 'RFC':
        model = RandomForestClassifier(n_estimators=100)
    else: #XGB
        le = preprocessing.LabelEncoder()
        model = XGBClassifier()
        cat_train_label = le.fit_transform(cat_train_label)

    model.fit(cat_train_set_scaled, cat_train_label)
    return model.predict(cat_user_vals_scaled), best_model
