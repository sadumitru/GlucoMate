from django.db import models
from users.models import User
from datetime import datetime


class Contact(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    subject = models.CharField(max_length=100)
    message = models.TextField()

    def __str__(self):
        return self.email


class Value(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    value = models.FloatField()
    date = models.DateTimeField()

    def __str__(self):
        return str(self.value)


class DiabetesClassRecord(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    systolic_bp = models.FloatField(null=True)
    diastolic_bp = models.FloatField(null=True)
    triglyceride = models.FloatField(null=True)
    hdl = models.FloatField(null=True)
    ldl = models.FloatField(null=True)
    gpt = models.FloatField(null=True)
    height = models.FloatField(null=True)
    weight = models.FloatField(null=True)
    urinary_glucose = models.FloatField(null=True)
    serum_creatinine = models.FloatField(null=True)
    egfr = models.FloatField(null=True)
    uric_pro = models.FloatField(null=True)
    eyeground = models.CharField(
        null=False,
        max_length=15,
        default="Not mentioned",
        choices=(
            ("Not mentioned", "Not mentioned"),
            ("Yes", "Yes"),
            ("No", "No")
        )
    )
    class_type = models.CharField(
        max_length=15,
        choices=(
            ('Normal', 'Normal'),
            ('Pre-diabetic', 'Pre-diabetic'),
            ('Diabetic', 'Diabetic')
        ),
        default='Normal')
    date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return str(self.class_type)
