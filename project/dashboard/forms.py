from email.policy import default
from django import forms

from scripts.forecasting import forecasting
from .models import Contact, Value, DiabetesClassRecord


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'email', 'subject', 'message']


class ValuesForm(forms.ModelForm):
    class Meta:
        model = Value
        fields = ['value', 'date']


class DiabetesClassRecordForm(forms.ModelForm):
    systolic_bp = forms.FloatField(
        required=False, label="Systolic Blood Pressure (mmHg)", min_value=0, max_value=300)
    diastolic_bp = forms.FloatField(
        required=False, label="Diastolic Blood Pressure (mmHg)", min_value=0, max_value=200)
    triglyceride = forms.FloatField(
        required=False, label="Triglyceride (mg/dL)", min_value=0, max_value=400)
    hdl = forms.FloatField(
        required=False, label="HDL (mg/dL)", min_value=0, max_value=200)
    ldl = forms.FloatField(
        required=False, label="LDL (mg/dL)", min_value=0, max_value=400)
    gpt = forms.FloatField(
        required=False, label="GPT (U/L)", min_value=0, max_value=300)
    height = forms.FloatField(
        required=False, label="Height (cm)", min_value=100, max_value=220)
    weight = forms.FloatField(
        required=False, label="Weight (kg)", min_value=40, max_value=300)
    urinary_glucose = forms.FloatField(
        required=False, label="Urinary Glucose (mg/dL)",  min_value=0, max_value=200)
    serum_creatinine = forms.FloatField(
        required=False, label="Serum Creatinine (mg/dL)", min_value=0, max_value=2)
    egfr = forms.FloatField(
        required=False, label="eGFR (%)", min_value=0, max_value=100)
    uric_pro = forms.FloatField(
        required=False, label="Urinary Protein (mmol/L)", min_value=0, max_value=200)
    eyeground = forms.ChoiceField(
        required=False, label="Eyeground",
        choices=(
            ("Not mentioned", "Not mentioned"),
            ("Yes", "Yes"),
            ("No", "No")
        ),
        widget=forms.Select(attrs={'class': 'form-select'})
    )

    class Meta:
        model = DiabetesClassRecord
        fields = [
            'systolic_bp',
            'systolic_bp',
            'diastolic_bp',
            'triglyceride',
            'hdl',
            'ldl',
            'gpt',
            'height',
            'weight',
            'urinary_glucose',
            'serum_creatinine',
            'egfr',
            'uric_pro',
            'eyeground'
        ]


class BloodWorkForm(DiabetesClassRecordForm):
    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop("request")
        except:
            pass
        super(BloodWorkForm, self).__init__(*args, **kwargs)

    def is_valid(self):
        counter = 0
        for key in self.request.keys():
            if self.request.get(key) != '':
                counter += 1
            if counter >= 6:
                return True

        return False

    def save(self, commit=True):
        obj = super(BloodWorkForm, self).save(commit=False)
        if self.request['systolic_bp']:
            obj.systolic_bp = self.request['systolic_bp']
        if self.request['systolic_bp']:
            obj.systolic_bp = self.request['systolic_bp']
        if self.request['diastolic_bp']:
            obj.diastolic_bp = self.request['diastolic_bp']
        if self.request['triglyceride']:
            obj.triglyceride = self.request['triglyceride']
        if self.request['hdl']:
            obj.hdl = self.request['hdl']
        if self.request['ldl']:
            obj.ldl = self.request['ldl']
        if self.request['gpt']:
            obj.gpt = self.request['gpt']
        if self.request['height']:
            obj.height = self.request['height']
        if self.request['weight']:
            obj.weight = self.request['weight']
        if self.request['urinary_glucose']:
            obj.urinary_glucose = self.request['urinary_glucose']
        if self.request['serum_creatinine']:
            obj.serum_creatinine = self.request['serum_creatinine']
        if self.request['egfr']:
            obj.egfr = self.request['egfr']
        if self.request['uric_pro']:
            obj.uric_pro = self.request['uric_pro']
        if self.request['eyeground']:
            obj.eyeground = self.request['eyeground']

        if commit:
            obj.save()

        return obj


    def get_uric_pro_cat(self, value):
        if value < 70:
            return 1
        elif value < 140:
            return 2
        elif value < 210:
            return 3
        else:
            return 4

    def get_urinary_gluc_cat(self, value):
        if value < 0.8:
            return 0
        elif value < 1:
            return 2
        else:
            return 3

    def get_bmi_cat(self, value):
        if value < 25:
            return 1
        elif value < 30:
            return 2
        else:
            return 3

    def get_blood_pressure_cat(self, systolic, diastolic):
        if systolic < 120 and diastolic < 80:
            return 1
        elif systolic < 139 and diastolic < 89:
            return 2
        else:
            return 3

    def get_trig_cat(self, value):
        if value < 200:
            return 1
        else:
            return 2

    def get_hdl_cat(self, value):
        if value >= 60:
            return 1
        elif value >= 45:
            return 2
        else:
            return 3

    def get_ldl_cat(self, value):
        if value < 130:
            return 1
        elif value < 160:
            return 2
        else:
            return 3

    def get_gpt_cat(self, value):
        if value < 56:
            return 1
        elif value < 180:
            return 2
        else:
            return 3

    def get_serum_creat_cat(self, value):
        if value < 1.9:
            return 1
        else:
            return 2

    def get_egfr_cat(self, value):
        if value > 60:
            return 1
        elif value > 20:
            return 2
        else:
            return 3

    def get_field_columns(self):
        values = {}
        categories = {}
        for column_name in self.request.keys():
            column_value = self.request.get(column_name)
            if column_value != '' and column_name != "csrfmiddlewaretoken":
                if column_name == "eyeground":
                    if column_value == 'Yes':
                        categories[column_name] = [1]
                    elif column_value == 'No':
                        categories[column_name] = [0]
                else:
                    values[column_name] = float(column_value)

        if "uric_pro" in values.keys():
            categories["uric_pro"] = [int(self.get_uric_pro_cat(values["uric_pro"]))]

        if "urinary_glucose" in values.keys():
            categories["urinary_glucose"] = [int(self.get_urinary_gluc_cat(values["urinary_glucose"]))]

        if "height" in values.keys() and "weight" in values.keys():
            bmi_value = values["weight"] / values["height"]
            categories["BMI_cat"] = [int(self.get_bmi_cat(bmi_value))]

        if "systolic_bp" in values.keys() and "diasrolic_bp" in values.keys():
            categories["blood_pressure"] = [int(self.get_blood_pressure_cat(
                float(values["systolic_bp"]), float(values["diastolic_bp"])))]

        if "triglyceride" in values.keys():
            categories["trig_category"] = [int(self.get_trig_cat(values["triglyceride"]))]

        if "hdl" in values.keys():
            categories["hdl_cat"] = [int(self.get_hdl_cat(values["hdl"]))]

        if "ldl" in values.keys():
            categories["ldl_cat"] = [int(self.get_ldl_cat(values["hdl"]))]

        if "gpt" in values.keys():
            categories["gpt_cat"] = [int(self.get_gpt_cat(values["gpt"]))]

        if "serum_creatinine" in values.keys():
            categories["serum_creatinine_cat"] = [int(
                self.get_serum_creat_cat(values["serum_creatinine"]))]

        if "egfr" in values.keys():
            categories["egfr_cat"] = [int(self.get_egfr_cat(values["egfr"]))]

        return categories

    class Meta:
        model = DiabetesClassRecord
        fields = [
            'systolic_bp',
            'systolic_bp',
            'diastolic_bp',
            'triglyceride',
            'hdl',
            'ldl',
            'gpt',
            'height',
            'weight',
            'urinary_glucose',
            'serum_creatinine',
            'egfr',
            'uric_pro',
            'eyeground'
        ]


class ForecastingForm(forms.Form):
    forecasting = forms.ChoiceField(
        choices=(
            (1, "Monthly"),
            (3, "Once every 3 months"),
            (6, "Once every 6 months"),
            (9, "Once every 9 months"),
            (12, "Yearly")
        ),
        label='Choose the time interval for monitoring HbA1C', 
        widget=forms.Select(attrs={"class": "form-select"})
    )
    
    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop("request")
        except:
            pass
        super(ForecastingForm, self).__init__(*args, **kwargs)
