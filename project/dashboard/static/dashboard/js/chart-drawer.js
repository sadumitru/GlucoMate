Chart.defaults.global.elements.line.fill = false;

function draw_history_chart(data, labels) {
  var lineData = data;
  var lineColors = ['#C29938']
  var barData = []
  var barColors = ['transparent']

  const colors = {
    'light_red': 'rgba(255, 0, 0, 0.2)',
    'red': 'rgba(255, 0, 0, 0.8)',
    'light_green': 'rgba(0, 128, 0, 0.2)',
    'green': 'rgba(0, 128, 0, 0.8)'
  }

  for (var i = 1; i <= lineData.length; i++) {
    var diff = Math.abs(lineData[i - 1] - lineData[i]);

    if (diff > 0.5) {
      barData.push(lineData[i - 1])
      lineColors.push(colors['red']);
      barColors.push(colors['light_red']);
    } else {
      barData.push(lineData[i - 1])
      lineColors.push(colors['green']);
      barColors.push(colors['light_green']);
    }
  }

  const data_chart = {
    labels: labels,
    datasets: [
      {
        data: barData,
        borderColor: '#C29938',
        backgroundColor: barColors,
        order: 1
      },
      {
        data: lineData,
        lineTension: 0.2,
        borderColor: '#C29938',
        backgroundColor: lineColors,
        pointRadius: 7,
        type: 'line',
        order: 0
      }
    ]
  };

  var config = {
    type: 'bar',
    data: data_chart,
    options: {
      responsive: true,
      legend: {
        display: false
      },
      title: {
        display: false
      },
      scales: {
        xAxes: [{ display: true, ticks: { fontSize: 15 }, barPercentage: 0.1 }],
        yAxes: [{ ticks: { beginAtZero: true } }],
      }
    },
  };

  var ctx = document.getElementById('history-chart').getContext('2d');
  window.myPie = new Chart(ctx, config);
}

function draw_small_variation_chart(data, labels) {
  var data = data;
  var barData = []
  var barColors = []

  const colors = {
    'red': 'rgba(255, 0, 0, 0.5)',
    'green': 'rgba(0, 128, 0, 0.5)'
  }

  for (var i = 1; i < data.length; i++) {
    var diff = (data[i] - data[i - 1]).toFixed(2);
    barData.push(diff)

    if (diff > 0.5 || diff < -0.5) {
      barColors.push(colors['red']);
    } else {
      barColors.push(colors['green']);
    }
  }

  labels.pop();

  const data_chart = {
    labels: labels,
    datasets: [
      {
        data: barData,
        borderColor: '#C29938',
        backgroundColor: barColors,
        order: 1
      }
    ]
  };

  var config = {
    type: 'bar',
    data: data_chart,
    options: {
      responsive: true,
      legend: {
        display: false
      },
      title: {
        display: false
      },
      scales: {
        xAxes: [{ display: true, ticks: { fontSize: 15 } }],
        yAxes: [{ ticks: { beginAtZero: true } }],
      }
    },
  };

  var ctx = document.getElementById('small-variation-chart').getContext('2d');
  window.myPie = new Chart(ctx, config);
}

function draw_forecasting_chart(data, labels) {
  var hba1c_data = data['hba1c'];
  var est_data = data['forecast'];

  var hba1c_lineColors = [];
  for (var i = 0; i < hba1c_data.length; i++) {
    hba1c_lineColors.push('#003399');
  }

  var est_lineColors = [];
  for (var i = 0; i < hba1c_data.length; i++) {
    est_lineColors.push('#C29938');
  }

  const data_chart = {
    labels: labels,
    datasets: [
      {
        label: 'HbA1c current values',
        data: hba1c_data,
        lineTension: 0.2,
        borderColor: '#b3ccff',
        backgroundColor: hba1c_lineColors,
        pointRadius: 7,
        type: 'line',
        order: 0
      },
      {
        label: 'HbA1c estimated values',
        data: est_data,
        lineTension: 0.2,
        borderColor: '#f4ebd7',
        backgroundColor: est_lineColors,
        pointRadius: 7,
        type: 'line',
        order: 1
      }
    ]
  };

  var config = {
    type: 'line',
    data: data_chart,
    options: {
      responsive: true,
      legend: {
        display: true
      },
      title: {
        display: false
      },
      scales: {
        xAxes: [{ display: true, ticks: { fontSize: 15 }, barPercentage: 0.1 }],
        yAxes: [{ ticks: { beginAtZero: true } }],
      }
    },
  };

  var ctx = document.getElementById('forecasting-chart').getContext('2d');
  window.myPie = new Chart(ctx, config);
}


$(function () {
  new PerfectScrollbar(document.getElementById('last-values'));
});
