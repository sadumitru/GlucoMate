
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.conf import settings
from django.contrib import messages
from django.views.generic import TemplateView
from datetime import datetime, timedelta
import pytz
from scripts import classifier, forecasting
from users.forms import User, UserLoginForm, DashChoosePatientForm
from users.models import Doctor
from .models import Value, DiabetesClassRecord
from .forms import DiabetesClassRecordForm, ContactForm, ForecastingForm, ValuesForm, BloodWorkForm

local = pytz.timezone(settings.TIME_ZONE)


def about(request):
    return render(request, 'dashboard/about-us.html')


class Homepage(TemplateView):
    template_name = 'dashboard/homepage.html'

    def redirect_authenticated_user(self, request):
        if request.user and request.user.is_authenticated:
            return render(request, 'dashboard/homepage.html')

    def get(self, request):
        form = UserLoginForm(request)
        self.redirect_authenticated_user(request)

        doctors_obj = Doctor.objects.all()[:5]
        doctors = []

        for doctor_obj in doctors_obj:
            profile = doctor_obj.user.profile

            doctors.append({
                'name': profile.first_name + " " + profile.last_name,
                'specialty': doctor_obj.specialty,
                'image': profile.image.name
            })

        context = {
            "form": form,
            "doctors": doctors,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = UserLoginForm(request)
        form.save()
        self.redirect_authenticated_user(request)
        return render(request, self.template_name, {"form": form})


def contact(request):
    sent = None
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            email_subject = f'New message from {form.cleaned_data["email"]}: {form.cleaned_data["subject"]}'
            email_message = form.cleaned_data['message']
            email_address = settings.EMAIL_HOST_USER
            email = EmailMessage(
                email_subject, email_message, to=[email_address]
            )
            email.send()
            sent = True
        else:
            sent = False
    else:
        form = ContactForm()

    return render(request, 'dashboard/contact.html', {'sent': sent})


def get_request_range(request):
    if 'start_date' in request and 'end_date' in request:
        start = local.localize(datetime.strptime(
            request['start_date'], '%Y-%m-%d'), is_dst=None)
        end = local.localize(datetime.strptime(
            request['end_date'], '%Y-%m-%d'), is_dst=None) + timedelta(hours=23, minutes=59, seconds=59)

        return {'start': start, 'end': end}
    return None


def get_last_records(user, no_of_records=10):
    values = DiabetesClassRecord.objects.filter(
        user=user).order_by('-date')[:no_of_records]
    data = []

    for record in values:
        data.append({
            'systolic_bp': record.systolic_bp,
            'diastolic_bp': record.diastolic_bp,
            'triglyceride': record.triglyceride,
            'hdl': record.hdl,
            'ldl': record.ldl,
            'gpt': record.gpt,
            'height': record.height,
            'weight': record.weight,
            'urinary_glucose': record.urinary_glucose,
            'serum_creatinine': record.serum_creatinine,
            'egfr': record.egfr,
            'uric_pro': record.uric_pro,
            'eyeground': record.eyeground,
            'class_type': record.class_type,
            'date': record.date.replace(tzinfo=pytz.utc).astimezone(local).strftime("%d %b %Y %H:%M"),
        })

    return data


def get_context(user, range=None):
    context = {}

    if range:
        recorded_values = Value.objects.filter(user=user, date__range=[
                                               range['start'], range['end']]).order_by('date')
        context.update({
            'date': {
                'start': range['start'].replace(tzinfo=pytz.utc).astimezone(local).strftime('%Y-%m-%d'),
                'end': range['end'].replace(tzinfo=pytz.utc).astimezone(local).strftime('%Y-%m-%d')
            }
        })
    else:
        recorded_values = Value.objects.filter(user=user).order_by('date')

    data = []
    labels = []

    for record in recorded_values:
        labels.append(record.date.replace(tzinfo=pytz.utc).astimezone(
            local).strftime("%d %b %Y %H:%M"))
        data.append(record.value)

    context.update({
        'labels': labels,
        'data': data,
    })

    return context


class UserNewValue(TemplateView):
    template_name = 'dashboard/new_value.html'

    def get(self, request):
        context = get_context(
            request.user, range=get_request_range(request.GET))
        return render(request, self.template_name, context)

    def post(self, request):
        form = ValuesForm(request.POST)

        if form.is_valid():
            obj = Value(
                user=request.user, value=form.cleaned_data['value'], date=form.cleaned_data['date'])
            obj.save()

        context = get_context(
            request.user, range=get_request_range(request.POST))
        return render(request, self.template_name, context)


class UserValues(TemplateView):
    def get(self, request):
        context = get_context(
            request.user, range=get_request_range(request.GET))

        return render(request, self.template_name, context)


class ForecastingValues(TemplateView):
    template_name = 'dashboard/forecasting.html'
    
    def get_time_interval(self, data):
        dt1 = datetime.strptime(data['labels'][0], "%d %b %Y %H:%M")
        dt2 = datetime.strptime(data['labels'][1], "%d %b %Y %H:%M")
        return round((dt2 - dt1).days / 30)
 
    def get(self, request):
        data = get_context(request.user)
        estim_data = None
        estim_labels = None
        
        if len(data['labels']) < 5:
            messages.error(request, 'At least 5 values are required.')
        else:
            time_interval = self.get_time_interval(data)
            estim_data, estim_labels = forecasting.forecasting(data, time_interval)

        context = {
            'data': estim_data,
            'labels': estim_labels,
        }
        return render(request, self.template_name, context)

class PatientDashboard(TemplateView):
    def get(self, request):
        dash_choose_patient_form = DashChoosePatientForm(user=request.user)
        patients = dash_choose_patient_form.get_choices_count()
        patients_unconfirmed = dash_choose_patient_form.get_choices(
            request.user, link_confirmed=False)
        context = {
            'dash_choose_patient_form': dash_choose_patient_form,
            'patients_count': patients,
            'patients_unconfirmed': patients_unconfirmed,
        }

        return render(request, self.template_name, context)

    def post(self, request):
        patient = User.objects.get(pk=request.POST['patients'])

        dash_choose_patient_form = DashChoosePatientForm(
            user=request.user, instance=request.POST)
        context = get_context(patient, range=get_request_range(request.POST))
        context.update({
            'dash_choose_patient_form': dash_choose_patient_form,
            'history_data': get_last_records(patient)
        })

        return render(request, self.template_name, context)


class Userclassification(TemplateView):
    template_name = "dashboard/classification.html"

    def get_diabetes_class(self, hba1c_score, model_name):
        diab_class = "Diabetic"

        if model_name == "XGB":
            hba1c_score -= 1

        if hba1c_score == 1:
            diab_class = "Normal"
        elif hba1c_score == 2:
            diab_class = "Pre-diabetic"
        return diab_class

    def get(self, request):
        form = BloodWorkForm()
        context = {
            'form': form,
            'history_data': get_last_records(request.user)
        }

        return render(request, self.template_name, context)

    def post(self, request):
        diabetes_class = None
        form = BloodWorkForm(request=request.POST)

        if not form.is_valid():
            messages.error(request, 'At least 5 values are required.')
        else:
            categories = form.get_field_columns()
            hba1c, model_name = classifier.classifier(categories)
            diabetes_class = self.get_diabetes_class(
                float(hba1c[0]), model_name
            )

            if diabetes_class != None:
                obj = form.save(commit=False)
                obj.user = self.request.user
                obj.class_type = diabetes_class
                obj.save()

        context = {
            'form': form,
            'diabetes_class': diabetes_class,
            'history_data': get_last_records(request.user)
        }

        return render(request, self.template_name, context)
