from django.urls import path
from .views import Homepage, UserValues, UserNewValue, PatientDashboard, Userclassification, ForecastingValues
from . import views

urlpatterns = [
    path('', Homepage.as_view(), name='dashboard-homepage'),
    path('about/', views.about, name='dashboard-about'),
    path('contact/', views.contact, name='dashboard-contact'),
    path('new_value/', UserNewValue.as_view(), name="dashboard-new_value"),
    path('history/', UserValues.as_view(template_name = 'dashboard/history.html'), name="dashboard-history"),
    path('forecasting', ForecastingValues.as_view(), name="dashboard-forecasting"),
    path('small_variation/', UserValues.as_view(template_name = 'dashboard/small_variation.html'), name="dashboard-small_variation"),
    path('classification/', Userclassification.as_view(), name="dashboard-classification"),
    path('patient_dashboards/', PatientDashboard.as_view(template_name = 'dashboard/patient_dashboards.html'), name="dashboard-patient_dashboards"),
]
