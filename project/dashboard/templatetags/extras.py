from django import template

register = template.Library()

@register.filter
def at(list, idx):
    try:
        return list[idx]
    except:
        return None

@register.filter
def limit(list, size):
    try:
        return list[:size]
    except:
        return None

@register.filter
def reverse(list):
    try:
        return list[::-1]
    except:
        return None

@register.filter
def get_level_tag(messages, tag):
    try:
        result = []
        for msg in messages:
            if msg.level_tag == tag:
                result.append(msg)
        return result
    except:
        return None

@register.filter
def get_bootstrap_level_tag_name(tag):
    try:
        if tag == 'error':
            return 'danger'
        elif tag == 'debug':
            return 'secondary'
        else:
            return tag
    except:
        return None
    
@register.filter
def get_level_tags(messages):
    try:
        result = []
        for msg in messages:
            if msg.level_tag not in result:
                result.append(msg.level_tag)
        return result
    except:
        return None
