from django.contrib import admin
from  django.contrib.auth.models import Group
from .models import Value, DiabetesClassRecord
from .forms import DiabetesClassRecordForm


class ValueConfig(admin.ModelAdmin):
    search_fields = ('user', 'value', 'date')
    ordering = ('-date',)
    list_filter = ('user',)
    list_display = ('id', 'user', 'value', 'date')
    fieldsets = (
        (None, {'fields': ('user', 'value',)}),
    )

    add_fieldsets = (
        (None, {'fields': ('value')})
    )

    readonly_fields = ["date"]


class DiabetesClassesConfig(admin.ModelAdmin):
    form = DiabetesClassRecordForm
    add_form = DiabetesClassRecordForm

    list_display = ['id', 'user', 'class_type', 'date']
    list_filter = ['class_type']
    fieldsets = (
        (None, {'fields': ('user',)}),
        ("Blood work", {'fields': ( 'systolic_bp', 'diastolic_bp', 'triglyceride', 'hdl', 'ldl', 'gpt', 'height', 'weight', 'urinary_glucose', 'serum_creatinine', 'egfr', 'uric_pro', 'eyeground',)}),
        ("Results", {'fields': ('class_type',)}),
        (None, {'fields': ('date',)}),
    )
    add_fieldsets = fieldsets

    search_fields = ['user', 'class_type']
    ordering = ['id']
    filter_horizontal = ()


admin.site.register(Value, ValueConfig)
admin.site.register(DiabetesClassRecord, DiabetesClassesConfig)
admin.site.unregister(Group)
