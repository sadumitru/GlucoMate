from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from users import views as user_views
from users.views import RegisterView, DoctorConnectConfirm


urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('profile/', user_views.profile, name='profile'),
    path('doctor/remove/<upkb64>/', user_views.remove_doctor, name='remove_doctor'),
    path('doctor/connect-confirm/<cpkb64>/<upkb64>/<token>/', DoctorConnectConfirm.as_view(), name='doctor_connect_confirm'),
    path('activate-account/<uidb64>/<token>/', user_views.activate, name='activate_account'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='dashboard-logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='users/password_reset.html'), name='password_reset'),
    path('password-reset-done/', auth_views.PasswordResetView.as_view(template_name='users/password_reset_done.html'), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'), name='password_reset_complete'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
