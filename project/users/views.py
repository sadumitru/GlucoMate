from django.shortcuts import render, redirect
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.contrib import messages
from django.views.generic import TemplateView
from django.utils.encoding import force_bytes

from users.models import Doctor, PatientsList
from .forms import UserRegisterForm, ProfileRegisterForm, ProfileUpdateForm, EmailUpdateForm, PasswordUpdateForm, ProfileChooseDoctorForm

UserModel = get_user_model()


class RegisterView(TemplateView):
    template_name = 'users/register.html'
    success_url = 'pending'

    def get_context_data(self, **kwargs):
        context = super(RegisterView, self).get_context_data(**kwargs)
        context['user_register_form'] = UserRegisterForm(
            data=self.request.POST if 'user_register_form' in self.request.POST else None,
        )
        context['profile_register_form'] = ProfileRegisterForm(
            data=self.request.POST if 'profile_register_form' in self.request.POST else None,
        )
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        if context['user_register_form'].is_valid() and context['profile_register_form'].is_valid():
            context['user_register_form'].save()
            context['profile_register_form'].save()
            messages.success(
                request, 'A new account was created. Email confirmation is needed. Please check your email!')
        else:
            for k in context['user_register_form'].errors.values():
                for v in k:
                    messages.error(request, v)
            for k in context['profile_register_form'].errors.values():
                for v in k:
                    messages.error(request, v)

        return self.render_to_response(context)


def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = UserModel._default_manager.get(id=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, 'users/account_activation_complete.html')
    else:
        return HttpResponse('Activation link is invalid!')


@login_required
def remove_doctor(request, upkb64):
    dr = None

    if request.method == 'GET':
        try:
            uid = urlsafe_base64_decode(upkb64).decode()
            dr = Doctor.objects.get(id=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            dr = None

        if dr is not None and PatientsList.objects.filter(doctor=dr, user=request.user).count() != 0:
            PatientsList.objects.get(doctor=dr, user=request.user).delete()
            messages.success(request, f'Your doctor was removed!')
    return redirect('profile')


@login_required
def profile(request):
    error_msg = ""
    if request.method == 'POST':
        email_update_form = EmailUpdateForm(
            request.POST, instance=request.user)
        password_update_form = PasswordUpdateForm(
            request.POST, instance=request.user)
        profile_update_form = ProfileUpdateForm(request.POST,
                                                request.FILES,
                                                instance=request.user.profile)
        profile_choose_doctor_form = ProfileChooseDoctorForm(request.POST,
                                                             request.FILES,
                                                             instance=request.user)

        if email_update_form.is_valid():
            email_update_form.save()
            messages.success(request, f'Your account has been updated!')

        if password_update_form.is_valid():
            if is_valid_pass(password_update_form):
                user = password_update_form.save()
                user.set_password(
                    password_update_form.cleaned_data.get('password1'))
                user.save()
                messages.success(request, f'Your account has been updated!')
                return redirect('profile')
            else:
                error_msg = "Passwords must match"

        if profile_update_form.is_valid():
            profile_update_form.save()
            messages.success(request, f'Your account has been updated!')

        if profile_choose_doctor_form.is_valid():
            profile_choose_doctor_form.save()
            messages.success(request, f'Your doctor has been updated!')

    else:
        email_update_form = EmailUpdateForm(instance=request.user)
        password_update_form = PasswordUpdateForm()
        profile_update_form = ProfileUpdateForm(instance=request.user.profile)
        profile_choose_doctor_form = ProfileChooseDoctorForm(
            instance=request.user)

    context = {
        'email_update_form': email_update_form,
        'password_update_form': password_update_form,
        'profile_update_form': profile_update_form,
        'profile_choose_doctor_form': profile_choose_doctor_form,
        'error_msg': error_msg
    }

    user_connection_requests = PatientsList.objects.filter(user=request.user)

    if user_connection_requests.count() != 0:
        assigned_doctors = Doctor.objects.filter(id__in=user_connection_requests.values('doctor_id'))
        context['doctors'] = []

        for dr in assigned_doctors:
            context['doctors'].append({
                'pk': urlsafe_base64_encode(force_bytes(dr.pk)),
                'connection_approved': user_connection_requests.get(doctor=dr).link_confirmation,
                'name': dr.user.profile.first_name + ' ' + dr.user.profile.last_name,
                'specialty': dr.specialty
            })

    return render(request, 'users/page-sections/profile-section.html', context)


class DoctorConnectConfirm(TemplateView):
    template_name = 'users/profile.html'
    success_url = 'profile'

    def get(self, request, *args, **kwargs):
        try:
            uid = urlsafe_base64_decode(kwargs['upkb64']).decode()
            cid = urlsafe_base64_decode(kwargs['cpkb64']).decode()
            user = UserModel._default_manager.get(pk=uid)
            patient_conn = PatientsList.objects.get(pk=cid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            patient_conn = None
        if patient_conn is not None and user is not None and default_token_generator.check_token(user, kwargs['token']):
            patient_conn.link_confirmation = True
            patient_conn.save()
            return render(request, 'users/connection_activation_complete.html')
        else:
            return HttpResponse('Activation link is invalid!')
