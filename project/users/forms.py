from multiprocessing.dummy import current_process
from django import forms
from .models import Doctor, PatientsList, Profile
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.utils.translation import gettext_lazy as _
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.conf import settings


User = get_user_model()


class UserRegisterForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email']

    def clean(self):
        '''
        Verify both passwords match.
        '''
        cleaned_data = super().clean()

        try:
            User.objects.get(email=cleaned_data['email'])
            self.add_error(
                'email', 'An account with this email already exists.')
        except:
            pass

        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        if password1 is not None and password1 != password2:
            self.add_error("password2", "Your passwords must match")
        return cleaned_data

    def send_confirmation_email(self, user):
        subject = 'Activate your account.'
        message = render_to_string('users/account_activation_email.html', {
            'user': user,
            'domain': settings.SITE_URL,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': default_token_generator.make_token(user),
        })
        email = EmailMessage(
            subject, message, to=[user.email]
        )
        email.send()

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserRegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.active = False

        if commit:
            user.save()

        self.send_confirmation_email(user)

        return user


class UserLoginForm(AuthenticationForm):
    email = forms.EmailField(label='email')
    password = forms.CharField(widget=forms.PasswordInput)


class ProfileRegisterForm(forms.ModelForm):
    """
    A form for creating new profiles.
    """
    first_name = forms.CharField()
    last_name = forms.CharField()
    phone_number = forms.CharField()
    cnp = forms.CharField()
    birthday = forms.DateField()
    address = forms.CharField()

    class Meta:
        model = Profile
        fields = ['first_name', 'last_name',
                  'phone_number', 'cnp', 'birthday', 'address']

    def clean(self):
        '''
        Verify form content (CNP valid, valid names, valid phone number, etc.).
        '''
        cleaned_data = super().clean()
        # cnp = cleaned_data.get("cnp")
        # if cnp is not None and len(cnp) != 13:
        #     self.add_error("cnp", "Invalid number of digits in CNP.")

        # TODO: Validate the CNP with checksum and also validate the other fields
        return cleaned_data

    def save(self, commit=True):
        profile = super(ProfileRegisterForm, self).save(commit=False)
        profile.user = User.objects.get(email=self.data["email"])

        if commit:
            profile.save()

        return profile


class EmailUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['email']


class PasswordUpdateForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['password1', 'password2']


class UserAdminCreationForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email']

    def clean(self):
        '''
        Verify both passwords match.
        '''
        cleaned_data = super().clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        if password1 is not None and password1 != password2:
            self.add_error("password2", "Your passwords must match")
        return cleaned_data

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['email', 'password', 'is_active',
                  'is_staff', 'is_superuser', 'last_login']

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class ProfileAdminCreationForm(forms.ModelForm):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    birthday = forms.DateField()
    address = forms.CharField(max_length=100)
    cnp = forms.CharField(max_length=13)
    phone_number = forms.CharField(max_length=15)
    # image = forms.ImageField()

    class Meta:
        model = Profile
        fields = ['first_name', 'last_name',
                  'birthday', 'address', 'cnp', 'phone_number']

    def clean(self):
        '''
        Verify profile.
        '''
        cleaned_data = super().clean()
        cnp = cleaned_data.get("cnp")
        # TODO: VALIDATE CNP & others (TBD later)
        # if cnp is not None and not cnp_valid():
        #     self.add_error("cnp", "Invalid CNP")
        return cleaned_data

    def save(self, commit=True):
        profile = super().save(commit=False)
        # add other fields if needed before saving the profile

        if commit:
            profile.save()
        return profile


class ProfileAdminChangeForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['first_name', 'last_name',
                  'birthday', 'address', 'cnp', 'phone_number']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']


class ProfileChooseDoctorForm(forms.Form):
    doctors = forms.ChoiceField(choices=[], label='')

    def get_choices(self):
        assigned_doctors = PatientsList.objects.filter(user=self.instance).values('doctor_id')
        doctors = []
        for doctor in Doctor.objects.exclude(id__in=assigned_doctors):
            doctors.append((doctor.pk, doctor.get_full_name()))

        if len(doctors) == 0:
            doctors.append((-1, 'There are no other doctors available.'))

        return doctors

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop("instance")
        super(ProfileChooseDoctorForm, self).__init__(*args, **kwargs)

        self.fields['doctors'].choices = self.get_choices()

    def send_confirmation_email(self, user, doctor, conn_pk):
        subject = 'Confirm connection.'
        message = render_to_string('users/connection_activation_email.html', {
            'doctor': doctor.user.profile,
            'user': user.profile,
            'domain': settings.SITE_URL,
            'cpk': urlsafe_base64_encode(force_bytes(conn_pk)),
            'upk': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': default_token_generator.make_token(user),
        })
        email = EmailMessage(
            subject, message, to=[doctor.user.email]
        )
        email.send()

    def save(self):
        doctor = Doctor.objects.get(pk=self.cleaned_data['doctors'])
        patient = None

        if PatientsList.objects.filter(doctor=doctor,user=self.instance).count() == 0:
            patient = PatientsList.objects.create(doctor=doctor,user=self.instance)
            self.fields['doctors'].choices = self.get_choices()
            self.send_confirmation_email(self.instance, doctor, patient.pk)

        return patient

    def clean(self):
        if int(self.cleaned_data['doctors']) < 0:
            raise forms.ValidationError("You are not allowed to update the doctor!")

class DateInput(forms.DateInput):
    input_type = 'date'

class DashChoosePatientForm(forms.Form):
    patients = forms.ChoiceField(choices=[], label='Patient', widget=forms.Select(attrs={"class": "form-select"}))
    start_date = forms.DateField(required=True, label='Start date', widget=DateInput())
    end_date = forms.DateField(required=True, label='End date', widget=DateInput())

    def get_choices(self, user, link_confirmed=True):
        dr = Doctor.objects.get(user=user)
        patient_list = PatientsList.objects.filter(doctor=dr, link_confirmation=link_confirmed)
        users = []

        current_patient = None
        try:
            if self.instance['patients']:
                current_patient = self.instance['patients']
        except:
            pass

        for patient in patient_list:
            if current_patient == str(patient.user.pk):
                users.insert(0,(str(patient.user.pk), patient.user.get_full_name()))
            else:
                users.append((str(patient.user.pk), patient.user.get_full_name()))

        return users

    def get_choices_count(self):
        return len(self.fields['patients'].choices)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        try:
            self.instance = kwargs.pop("instance")
        except:
            pass
        super(DashChoosePatientForm, self).__init__(*args, **kwargs)

        self.fields['patients'].choices = self.get_choices(self.user)

        try:
            if self.instance['start_date'] and self.instance['end_date']:
                self.fields['start_date'].initial = self.instance['start_date']
                self.fields['end_date'].initial = self.instance['end_date']
        except:
            pass
