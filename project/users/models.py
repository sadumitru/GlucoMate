from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.tokens import default_token_generator
from tkinter import CASCADE
from PIL import Image


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, password=None, is_superuser=False, is_staff=False, is_active=False):
        if not email:
            raise ValueError(_('Please enter an email!'))
        if not password:
            raise ValueError(_('Please enter a password!'))

        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.is_active = is_active
        user.is_staff = is_staff
        user.is_superuser = is_superuser
        user.is_doctor = False
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_staff=True,
            is_active=True
        )
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_superuser=True,
            is_staff=True,
            is_active=True
        )
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True, max_length=255)

    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_doctor = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return f'User=[id:{self.id} email:{self.email}, active:{self.is_active}, staff:{self.is_staff}, doctor:{self.is_doctor}]'

    def get_full_name(self):
        return '%s %s' % (self.profile.first_name, self.profile.last_name)

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True


class Profile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.DateField(null=False)
    address = models.CharField(max_length=100)
    cnp = models.CharField(max_length=13)
    phone_number = models.CharField(max_length=15, null=False)
    registered_date = models.DateTimeField(default=timezone.now)
    image = models.ImageField(default='default.jpeg', upload_to='profile_pics')

    USERNAME_FIELD = 'user'
    REQUIRED_FIELDS = ['first_name', 'last_name',
                       'birthday', 'address', 'cnp', 'phone_number']

    def __str__(self):
        return f'Profile=[user:{self.user}, fname:{self.first_name}, lname:{self.last_name}, bday:{self.birthday}, addr:{self.address}, cnp:{self.cnp}, phone:{self.phone_number}, registered:{self.registered_date}, img:{self.image}]'

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (img.height, img.width)
            img.thumbnail(output_size)
            img.save(self.image.path)


class Doctor(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    specialty = models.CharField(max_length=100)

    def __str__(self):
        return f'Doctor=[{self.user}, spec:{self.specialty}]'

    def get_full_name(self):
        profile = Profile.objects.get(user=self.user)
        return 'Dr. %s %s - %s specialist' % (profile.first_name, profile.last_name, self.specialty)

    def save(self, *args, **kwargs):
        super(Doctor, self).save(*args, **kwargs)


class PatientsList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    doctor = models.ForeignKey(
        Doctor, on_delete=models.CASCADE)
    link_confirmation = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(PatientsList, self).save(*args, **kwargs)

    def __str__(self):
        return f'PatientsList=[{self.user}, dr:{self.doctor}, link_confirmation:{self.link_confirmation}]'
