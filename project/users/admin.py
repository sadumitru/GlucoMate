from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import Doctor, PatientsList, Profile
from .forms import UserAdminCreationForm, UserAdminChangeForm, ProfileAdminChangeForm, ProfileAdminCreationForm

User = get_user_model()

class UserAdminConfig(BaseUserAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    list_display = ['id', 'email', 'is_superuser', 'is_staff', 'is_active', 'is_doctor']
    list_filter = ['is_superuser', 'is_staff', 'is_active', 'is_doctor']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_superuser', 'is_staff', 'is_active', 'is_doctor',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ['email']
    ordering = ['id']
    filter_horizontal = ()

class ProfileAdminConfig(BaseUserAdmin):
    form = ProfileAdminChangeForm
    add_form = ProfileAdminCreationForm

    list_display = ['id', 'first_name', 'last_name', 'cnp', 'phone_number']
    list_filter = []
    fieldsets = (
        (None, {'fields': ('user',)}),
        ('Name', {'fields': ('first_name', 'last_name',)}),
        ('Details', {'fields': ('cnp', 'birthday', 'phone_number', 'address', 'image',)}),
    )
    add_fieldsets = (
        (None, {'fields': ('user',)}),
        ('Name', {'fields': ('first_name', 'last_name',)}),
        ('Details', {'fields': ('cnp', 'birthday', 'phone_number', 'address', 'image',)}),
    )

    search_fields = ['first_name', 'last_name', 'cnp', 'phone_number', 'address']
    ordering = ['id']
    filter_horizontal = ()

    readonly_fields = ["registered_date"]

class DoctorAdminConfig(admin.ModelAdmin):
    search_fields = ('user', 'specialty',)
    ordering = ('id',)
    list_filter = ('specialty',)
    list_display = ('id', 'user', 'specialty',)
    filter_horizontal = ()
    fieldsets = (
        (None, {'fields': ('user', 'specialty',)}),
    )

    add_fieldsets = (
        (None, {'fields': ('user', 'specialty',)}),
    )


class PatientsListAdminConfig(admin.ModelAdmin):
    search_fields = ('user', 'doctor', 'link_confirmation',)
    ordering = ('id',)
    list_filter = ('doctor', 'link_confirmation',)
    list_display = ('id', 'user', 'doctor', 'link_confirmation',)
    filter_horizontal = ()
    fieldsets = (
        (None, {'fields': ('user', 'doctor', 'link_confirmation',)}),
    )

    add_fieldsets = (
        (None, {'fields': ('user', 'doctor', 'link_confirmation',)}),
    )


admin.site.register(User, UserAdminConfig)
admin.site.register(Doctor, DoctorAdminConfig)
admin.site.register(PatientsList, PatientsListAdminConfig)
admin.site.register(Profile, ProfileAdminConfig)
