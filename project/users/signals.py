from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Doctor, User


@receiver(post_save, sender=User)
def create_doctor(sender, instance, created, **kwargs):
    if not created and instance.is_doctor:
        if Doctor.objects.filter(user=instance).count() == 0:
            Doctor.objects.create(user=instance)

@receiver(post_save, sender=User)
def delete_doctor(sender, instance, created, **kwargs):
    if not created and not instance.is_doctor:
        if Doctor.objects.filter(user=instance).count() != 0:
            Doctor.objects.get(user=instance).delete()

@receiver(post_save, sender=User)
def save_doctor(sender, instance, **kwargs):
    if instance.is_doctor:
        instance.doctor.save()
